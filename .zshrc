setopt HIST_EXPIRE_DUPS_FIRST
setopt EXTENDED_HISTORY
setopt AUTO_CD

autoload -Uz promptinit add-zsh-hook
autoload -U colors && colors
promptinit

export EDITOR="vim"
export PATH=$PATH:~/scripts:~/.config/bspwm/panel:~/.gem/ruby/2.2.0/bin
export XDG_CONFIG_HOME="$HOME/.config"
HISTFILE=$HOME/.zsh_history
HISTSIZE=10000
SAVEHIST=8000

alias xrdb='xrdb ~/.Xresources'
alias :q='exit'
alias ls='ls --color=auto'
alias make='make -j5'

if [ "$TERM" != "linux" ] && [ -z "$SSH_CLIENT" ]; then
	#PROMPT="%{$fg_no_bold[blue]%}\ %{$reset_color%}"
    PROMPT="%{$fg_no_bold[magenta]%}\ %{$reset_color%} "
    RPROMPT="%{$fg_no_bold[magenta]%}%~%{$reset_color%}"
else
	PROMPT="%n@%m %~ %# "
fi

precmd() {
    if [ "$TERM" != "linux" ]; then
		print -Pn "\e]0;%~\a"
	fi
}
