REFRESH_DELAY=0.6

update() {
    WINF=$(bspc query -W -w focused);

    for i in $(bspc query -W -d $MONF); do
        title $i;
    done
}

title() {
    [[ "$1" == "$WINF" ]] && echo -n "A" || echo -n "X";

    title="$(xtitle -t "$1")"

    echo -n "$title"
}

while :; do
    update
    MONF=$(bspc query --monitor ^1 -T | grep " - \*" | grep -oE "[0-9]/i+");
    WINC="$(bspc query -H -d $MONF | tail -n 1)";
    if [[ "$WINA" != "$WINC" ]]; then
        WINA=$WINC
        echo "T$(update)"
    fi
done
